package org.example;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class NoArgumentsTest {

    @Test
    public void testNoArguments() {
        String expectedOutput = "No arguments provided.";

        String actualOutput = getOutput();

        assertEquals(expectedOutput, actualOutput);
    }

    private String getOutput() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        SortingApp.main(new String[]{});

        System.setOut(originalOut);

        return outputStream.toString().trim();
    }
}