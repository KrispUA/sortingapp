package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.io.PrintStream;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ArgumentsTest {

    private String[] args;
    private int[] expected;

    public ArgumentsTest(String[] args, int[] expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{"5", "2", "9", "1", "7", "4", "6", "3", "8", "0"}, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}},
                {new String[]{"5","25","50","400","700","100","1000","125","200","75", }, new int[]{5, 25, 50, 75, 100, 125, 200, 400, 700, 1000}},
                {new String[]{"-40", "0", "173", "-25", "87", "91", "24", "-89", "900", "44"}, new int[]{-89, -40, -25, 0, 24, 44, 87, 91, 173, 900}},
                {new String[]{"-40", "0", "173", "-25", "87", "91", "24", "-89", "900", "44", "-105", "1500", "1890"}, new int[]{-105, -89, -40, -25, 0, 24, 44, 87, 91, 173, 900, 1500, 1890}},
                {new String[]{"-40", "0", "173", "-25", "87", "91", "24", "-89", "900", "44", "-105"}, new int[]{-105, -89, -40, -25, 0, 24, 44, 87, 91, 173, 900}},
                {new String[]{"-40", "0", "173", "-25", "87", "91", "24", "-89", "900", "44", "-105", "1500"}, new int[]{-105, -89, -40, -25, 0, 24, 44, 87, 91, 173, 900, 1500}},
                {new String[]{"3"}, new int[]{3}},
                {new String[]{"72"}, new int[]{72}},
                {new String[]{"111"}, new int[]{111}},
        });
    }

    @Test
    public void testArguments() {
        SortingApp.main(args);
        String[] outputLines = getOutput().split(System.lineSeparator());
        int[] actual = new int[outputLines.length];
        for (int i = 0; i < outputLines.length; i++) {
            actual[i] = Integer.parseInt(outputLines[i]);
        }
        assertArrayEquals(expected, actual);
    }

    private String getOutput() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        SortingApp.main(args);

        System.setOut(originalOut);

        return outputStream.toString().trim();
    }
}